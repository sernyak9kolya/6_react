import React from 'react';
import propTypes from 'prop-types';
import './MessageInput.css';
import { ChatConsumer } from '../../containers/Chat/Chat';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };
  }

  handleInput = e => {
    const text = e.target.value;
    this.setState(() => ({ text }));
  };

  resetInput = () => {
    this.setState(() => ({ text: '' }));
  };

  render() {
    const { text } = this.state;

    return (
      <ChatConsumer>
        {({ addMessage }) => (
          <form className="d-flex" onSubmit={e => {
            e.preventDefault();
            addMessage(text);
            this.resetInput();
          }}>
            <input
              type="text"
              value={text}
              placeholder="Enter your message"
              className="messageInput form-control form-control-lg"
              onChange={this.handleInput}
            />
            <button type="submit" className="messageInput__btn btn btn-primary">
              Send <i className="fa fa-send"></i>
             </button>
          </form>
        )}
      </ChatConsumer>
    );
  };
}

MessageInput.propTypes = {
  messages: propTypes.arrayOf(propTypes.object)
};

export default MessageInput;
