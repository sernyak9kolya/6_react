import React from 'react';

class Spinner extends React.Component {
  render() {
    return (
      <div className="d-flex justify-content-center align-items-center h-100">
        <div className="spinner-border text-primary" role="status" style={{ width: '3rem', height: '3rem' }}>
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  };
}

export default Spinner;
