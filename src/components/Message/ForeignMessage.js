import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';

class ForeignMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLiked: false
    };
  }

  handleLike() {
    this.setState(state => ({
      isLiked: !state.isLiked
    }))
  };

  render() {
    const { isLiked } = this.state;
    const { message } = this.props;
    const { text, createdAt, user, avatar } = message;

    return (
      <div className="message message-foreign" onClick={() => this.handleLike()}>
        <div className="message__content d-flex alert alert-primary">
          <div className="message__avatarWrapper">
            <img src={avatar} alt={user} className="message__avatar" />
          </div>
          <div>
            <div className="message__text">{text}</div>
            <span className="message__createdAt">{moment(createdAt).format('DD:mm')}</span>
            { isLiked ? <i className="message__like fa fa-thumbs-up"></i> : null }
          </div>
        </div>
        <div className="message__actions"></div>
      </div>
    );
  };
}

ForeignMessage.propTypes = {
  message: propTypes.object
};

export default ForeignMessage;
