import React from 'react';
import propTypes from 'prop-types';
import OwnMessage from './OwnMessage';
import ForeignMessage from './ForeignMessage';
import { CURRENT_USER } from '../../config';
import './Message.css';

class Message extends React.Component {
  render() {
    const { message } = this.props;

    return message.userId === CURRENT_USER.id
        ? <OwnMessage message={message} />
        : <ForeignMessage message={message} />;
  };
}

Message.propTypes = {
  message: propTypes.object
};

export default Message;
