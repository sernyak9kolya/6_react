import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import { ChatConsumer } from '../../containers/Chat/Chat';

class OwnMessage extends React.Component {
  render() {
    const { message } = this.props;
    const { text, createdAt } = message;

    return (
      <ChatConsumer>
        {({ editMessage, deleteMessage}) => (
          <div className="message message-own align-self-end">
            <div className="message__content alert alert-light">
              <div className="message__text">{text}</div>
              <div className="message__createdAt">{moment(createdAt).format('HH:mm')}</div>
            </div>
            <div className="message__actions">
              <i className="message__icon fa fa-pencil" onClick={() => editMessage(message)}></i>
              <i className="message__icon fa fa-trash" onClick={() => deleteMessage(message)}></i>
            </div>
          </div>
        )}
      </ChatConsumer>
    );
  };
}

OwnMessage.propTypes = {
  message: propTypes.object
};

export default OwnMessage;
