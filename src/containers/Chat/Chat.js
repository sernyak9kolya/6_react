import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { callApi } from '../../helpers/apiHelper';
import ChatView from './ChatView';
import { CURRENT_USER } from '../../config';

const API_URL = 'https://api.npoint.io/b919cb46edac4c74d0a8';

const {
  Provider: ChatProvider,
  Consumer: ChatConsumer
} = React.createContext();

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      messages: []
    };
  }

  addMessage = text => {
    if (!text) {
      return;
    }

    const newMessage = {
      text,
      id: uuidv4(),
      userId: CURRENT_USER.id,
      user: CURRENT_USER.name,
      avatar: CURRENT_USER.avatar,
      createdAt: new Date().toISOString(),
      editedAt: ''
    };

    this.setState(state => ({
      messages: [
        ...state.messages,
        newMessage
      ]
    }));
  };

  editMessage = message => {
    const { id, text } = message;
    const editedText = prompt('Enter your message', text);

    if (editedText === null) {
      return;
    }

    const editedMessage = {
      ...message,
      editedAt: new Date().toISOString(),
      text: editedText
    };

    this.setState(state => {
      const messageIdx = state.messages.findIndex(msg => msg.id === id);
      const messages = [
        ...state.messages.slice(0, messageIdx),
        editedMessage,
        ...state.messages.slice(messageIdx + 1),
      ];

      return { messages };
    });
  };

  deleteMessage = message => {
    const { id } = message;
    const confirmed = window.confirm('Do you really want to delete the message?');

    if (!confirmed) {
      return;
    }

    this.setState(state => {
      const messages = state.messages.filter(msg => msg.id !== id);
      return { messages };
    });
  };

  async componentWillMount() {
    this.setState(() => ({ isLoading: true }));
    try {
      const messages = await callApi(API_URL);
      this.setState(() => ({ messages }));
    } catch (error) {
      throw error;
    } finally {
      this.setState(() => ({ isLoading: false }));
    }
  }

  render() {
    const { isLoading, messages } = this.state;
    const chatProviderValue = {
      addMessage: this.addMessage,
      editMessage: this.editMessage,
      deleteMessage: this.deleteMessage
    };

    return (
      <ChatProvider value={chatProviderValue}>
        <ChatView messages={messages} isLoading={isLoading} />
      </ChatProvider>
    );
  };
}

export default Chat;
export { ChatConsumer };
