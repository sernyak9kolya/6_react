const callApi = (url, method = 'GET') => {
  const options = {
    method
  };

  return fetch(url, options)
    .then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load')))
    .then(resource => resource)
    .catch(error => {
      throw error;
    })

};

export { callApi };
