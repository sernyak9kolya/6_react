import React from 'react';
import Chat from './containers/Chat/Chat';
import ErrorBoundary from './components/ErrorBoundary';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="app">
        <header className="header">
          <div className="header__container container">
            <h4 className="header__logo">React Chat</h4>
          </div>
        </header>
        <div className="chatWrapper">
          <ErrorBoundary>
            <Chat />
          </ErrorBoundary>
        </div>
        <footer className="footer">
          <div className="footer__container container">
            <span className="footer__copyright">2020 &copy; React Chat</span>
          </div>
        </footer>
      </div>
    );
  };
}

export default App;
